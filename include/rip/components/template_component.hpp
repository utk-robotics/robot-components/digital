#ifndef TEMPLATE_COMPONENT_TEMPLATE_COMPONENT_HPP
#define TEMPLATE_COMPONENT_TEMPLATE_COMPONENT_HPP

#include <rip/robot_component.hpp>

namespace rip::components 
{

    class TemplateComponent : public RobotComponent 
    {
        public:

            TemplateComponent(const std::string& name, const nlohmann::json& config,
                    std::shared_ptr<emb::host::EmbMessenger> emb,
                    std::shared_ptr<std::unordered_map<std::string, 
                        std::shared_ptr<RobotComponent> > > comps);

            virtual std::vector<std::string> diagnosticCommands() const override;

            virtual void stop() override;
    };

}

#endif // TEMPLATE_COMPONENT_TEMPLATE_COMPONENT_HPP
